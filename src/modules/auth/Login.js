import React,{useState} from 'react';
import {Link} from 'react-router-dom'
import {authRequirements} from '../common/constants'
import Title from '../common/components/Title'
import Input from '../common/hookforms/Input'
import Submit from '../common/hookforms/Submit'
import Password from '../common/hookforms/Password'
import CenteredScreen from '../layout/CenteredScreen'
import { Form, Button, ModalTitle, ModalFooter, ModalBody } from 'react-bootstrap'
import useForm from 'react-hook-form'
import styled from 'styled-components'
import AuthBanner from './AuthBanner'
import Popup from "reactjs-popup";
import '../styles/scss/login.scss'
const Wrapper = styled(CenteredScreen)`
  form{
    width:300px;
    text-align:left;
    margin:0 auto 15px;
    .submit{
      margin-top:42px;
      text-align:center;
    }
  }
`

const StyledPopup = styled(Popup)`
  // use your custom style for ".popup-overlay"
  &-overlay {
    background: rgba(255,255,255,0.5);
  }
  // use your custom style for ".popup-content"
  &-content {
    width: 100%;
    background:white;
    display: flex;
    flex-direction: column;
    padding: 30px 20px 30px;
    border-radius: 30px;
  }
`;

const Login = ({location, history}) => {
  const form = useForm({})
  const onSubmit = (val) =>{}
  const [modalState,setmodalState]=useState(false);
  const [agreeState,setagreeState] = useState(false);
  const [infoState,setinfoState] = useState(false);
  return (<Wrapper className="screen-login">
    <Title title="Log in"/>
    <StyledPopup open={modalState} position='top center' closeOnDocumentClick={false} closeOnEscape={false}>
      <ModalTitle style={{display:'flex',justifyContent:'center',alignItems:'center'}}>Terms & Privacy Policy</ModalTitle>
      <ModalBody className={'modalBody'}>
        <div className={'card'}>
          <h3>Statment of Intent</h3>
          <p>{'We want you to know exactly how our services work and why we need your details.\n Reviewing our policy will help you continue using the app with peace of mind.'}</p>
        </div>
        <div className={'bodyText'}>
          <div className={'navigation'}>
            <Button disabled={infoState} onClick={()=>{setinfoState(!infoState)}}>
              Terms of Use
            </Button>
            <Button disabled={!infoState}  onClick={()=>{setinfoState(!infoState)}}>
              Privacy Policy
            </Button>
          </div>
          {infoState?(<div className={'text'}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra diam vel pulvinar commodo. Donec rutrum justo pretium ligula fermentum dictum sed id eros. Sed sodales rutrum leo. Mauris venenatis vestibulum pretium. Praesent convallis nisi dolor. Praesent auctor nibh risus, eu porttitor mauris posuere ut. Aliquam laoreet nibh id odio egestas sagittis. Phasellus non nibh justo. Nulla elit sapien, egestas in mauris ut, consectetur rhoncus neque. Phasellus posuere posuere sagittis. Vestibulum varius diam quis orci ornare convallis. Etiam aliquet rutrum ex, eu ullamcorper ipsum scelerisque sed.

            Morbi eu diam in nisl euismod hendrerit a vel erat. Sed mattis vel ligula id finibus. Cras pellentesque ante leo, sed sagittis purus ullamcorper sit amet. Sed pretium dictum dui. Praesent vitae ante consequat, aliquet sapien mollis, tempor dui. In luctus, est in condimentum efficitur, risus ante viverra libero, id faucibus lorem mauris sit amet tellus. Aliquam finibus, dolor nec imperdiet efficitur, leo urna egestas enim, nec semper purus odio at massa.

            Praesent at vehicula arcu, in cursus est. Aenean vulputate, dolor facilisis mollis mollis, turpis odio suscipit neque, vitae congue eros justo dignissim mauris. Phasellus lacinia luctus orci sit amet porta. Quisque eu odio augue. Phasellus feugiat tortor vitae justo interdum pharetra. Morbi blandit condimentum imperdiet. Nulla sollicitudin vel urna non consequat. In consequat nibh ut felis facilisis, vitae pellentesque dolor laoreet. Morbi ullamcorper accumsan purus, nec imperdiet est condimentum eleifend. Phasellus tincidunt sem sed magna facilisis, sed elementum libero pellentesque. Proin faucibus dapibus nulla, quis vestibulum mi fermentum nec. Ut eu lectus ut quam vulputate blandit et in purus. Praesent condimentum lacus eros, in rutrum nisl mollis eu. Aliquam augue ex, pellentesque sed nunc non, sodales fermentum sapien. Vivamus a posuere metus, sed mattis tortor. Nulla faucibus erat a ligula blandit convallis.

            Ut vehicula id nisi ac sodales. Praesent nisi libero, euismod et odio eget, tincidunt porttitor urna. Aliquam sed ornare erat. Fusce arcu mauris, semper non imperdiet non, suscipit a est. Fusce tincidunt tortor at nisi malesuada eleifend. Vivamus vel interdum magna, vel sodales tellus. Nam finibus hendrerit bibendum. Etiam vestibulum sodales lorem, sit amet tempus massa luctus vel. Cras fermentum orci sem, in scelerisque ex commodo quis. Aliquam a consequat nisl.

            Pellentesque lacinia eget nibh id sagittis. Aliquam vitae tempus turpis. Vivamus lobortis elit elit, at dictum lorem ultricies non. Donec vel porttitor libero. Suspendisse ut finibus mauris. Nulla pellentesque id sem in tempus. In hac habitasse platea dictumst. Aenean dignissim lacinia elit sit amet consequat. Fusce sollicitudin fermentum tellus vitae varius. Donec ac porta urna.
          </div>):(   <div className={'text'}>
            <p>
              <h3>What personal Information We Collect</h3>
              Depending on how you engage with Cogniss Services, we collect different kind of information from or about you
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra diam vel pulvinar commodo. Donec rutrum justo pretium ligula fermentum dictum sed id eros. Sed sodales rutrum leo. Mauris venenatis vestibulum pretium. Praesent convallis nisi dolor. Praesent auctor nibh risus, eu porttitor mauris posuere ut. Aliquam laoreet nibh id odio egestas sagittis. Phasellus non nibh justo. Nulla elit sapien, egestas in mauris ut, consectetur rhoncus neque. Phasellus posuere posuere sagittis. Vestibulum varius diam quis orci ornare convallis. Etiam aliquet rutrum ex, eu ullamcorper ipsum scelerisque sed.

              Morbi eu diam in nisl euismod hendrerit a vel erat. Sed mattis vel ligula id finibus. Cras pellentesque ante leo, sed sagittis purus ullamcorper sit amet. Sed pretium dictum dui. Praesent vitae ante consequat, aliquet sapien mollis, tempor dui. In luctus, est in condimentum efficitur, risus ante viverra libero, id faucibus lorem mauris sit amet tellus. Aliquam finibus, dolor nec imperdiet efficitur, leo urna egestas enim, nec semper purus odio at massa.

              Praesent at vehicula arcu, in cursus est. Aenean vulputate, dolor facilisis mollis mollis, turpis odio suscipit neque, vitae congue eros justo dignissim mauris. Phasellus lacinia luctus orci sit amet porta. Quisque eu odio augue. Phasellus feugiat tortor vitae justo interdum pharetra. Morbi blandit condimentum imperdiet. Nulla sollicitudin vel urna non consequat. In consequat nibh ut felis facilisis, vitae pellentesque dolor laoreet. Morbi ullamcorper accumsan purus, nec imperdiet est condimentum eleifend. Phasellus tincidunt sem sed magna facilisis, sed elementum libero pellentesque. Proin faucibus dapibus nulla, quis vestibulum mi fermentum nec. Ut eu lectus ut quam vulputate blandit et in purus. Praesent condimentum lacus eros, in rutrum nisl mollis eu. Aliquam augue ex, pellentesque sed nunc non, sodales fermentum sapien. Vivamus a posuere metus, sed mattis tortor. Nulla faucibus erat a ligula blandit convallis.

              Ut vehicula id nisi ac sodales. Praesent nisi libero, euismod et odio eget, tincidunt porttitor urna. Aliquam sed ornare erat. Fusce arcu mauris, semper non imperdiet non, suscipit a est. Fusce tincidunt tortor at nisi malesuada eleifend. Vivamus vel interdum magna, vel sodales tellus. Nam finibus hendrerit bibendum. Etiam vestibulum sodales lorem, sit amet tempus massa luctus vel. Cras fermentum orci sem, in scelerisque ex commodo quis. Aliquam a consequat nisl.

              Pellentesque lacinia eget nibh id sagittis. Aliquam vitae tempus turpis. Vivamus lobortis elit elit, at dictum lorem ultricies non. Donec vel porttitor libero. Suspendisse ut finibus mauris. Nulla pellentesque id sem in tempus. In hac habitasse platea dictumst. Aenean dignissim lacinia elit sit amet consequat. Fusce sollicitudin fermentum tellus vitae varius. Donec ac porta urna.
            </p>
          </div>)
          }_
        </div>
      </ModalBody>
      <ModalFooter style={{display:'flex',justifyContent:'center',alignItems:'center'}}>
        <div style={{display:'flex', flexDirection:'column'}}>
          <div style={{display:'flex',flexDirection:'row'}}>
            <input type={'checkbox'} onChange={(e)=>{setagreeState(e.target.checked)}}/>
            <span style={{padding: '2px'}}>I have read and agree to the Terms and Privacy Policy</span>
          </div>
          <div style={{display:'flex',flexDirection:'row'}}>
          <Button onClick={()=>{setmodalState(false)}}>Cancel</Button>
          <Button onClick={()=>{history.push('/register')}} disabled={!agreeState}>Agree</Button>
          </div>
        </div>
      </ModalFooter>

    </StyledPopup>

    <div className="container-fluid">
      <AuthBanner/>
      <Form horizontal onSubmit={form.handleSubmit(onSubmit)}>
        <Input name="username" form={form} label="Username" minLength={authRequirements.USERNAME_MIN} required/>
        <Password name="password" form={form} label="Password" required/>
        <Submit className="submit" label="Sign in" form={form} primary/>
      </Form>
      <p>
        <Link to="forgot" className="btn-forgot">Forgot your username or password?</Link>
      </p>
      <p>
        Don't have an account yet? &nbsp;
        <Link onClick={()=>{setmodalState(true)}} className="btn-registration">Create an account?</Link>
      </p>
    </div>
  </Wrapper>)
}
export default Login
