import dotProp from 'dot-prop';
export function getValue(form, name){
  const vals = form.getValues()
  let val = vals[name]
  const dotName = name.split('[').join('.').split(']').join('')
  if(val === undefined) val = dotProp.get(vals, dotName)
  return val
}
